baseURL: "https://livres.dans-les-nuages.fr/"
title: Livres
paginate: 5
theme: PaperMod

taxonomies:
  genre: genres
  auteur: auteurs
  editeur: editeurs
  tag: tags
  serie: series
    
enableRobotsTXT: true
buildDrafts: false
buildFuture: false
buildExpired: false

minify:
  disableXML: true
  minifyOutput: true

params:
  env: production # to enable google analytics, opengraph, twitter-cards and schema.
  title: Livres
  description: "Mes lectures"
  keywords: [Blog, Livres]
  author: Moi
  images: ["/img/livres.webp"]
  DateFormat: "2 January 2006"
  defaultTheme: light # dark, light
  disableThemeToggle: false

  ShowReadingTime: true
  ShowShareButtons: true
  ShowPostNavLinks: true
  ShowBreadCrumbs: true
  ShowCodeCopyButtons: false
  ShowWordCount: true
  ShowRssButtonInSectionTermList: true
  UseHugoToc: true
  disableSpecial1stPost: false
  disableScrollToTop: false
  comments: false
  hidemeta: false
  hideSummary: false
  showtoc: false
  tocopen: false

  assets:
    disableHLJS: true # to disable highlight.js
    disableFingerprinting: true
    favicon: "/img/favicon.png"
    favicon16x16: "/img/favicon16.png"
    favicon32x32: "/img/favicon32.png"
    apple_touch_icon: "/img/favicon.png"
    safari_pinned_tab: "/img/favicon.png"

  label:
    text: "Home"
    icon: /apple-touch-icon.png
    iconHeight: 35

  # profile-mode
  profileMode:
    enabled: false # needs to be explicitly set
    title: Livres
    subtitle: "Mes lectures"
    imageUrl: "/img/favicon.png"
    imageWidth: 120
    imageHeight: 120
    imageTitle: "livres.dans-les-nuages.fr"
    buttons:
      - name: Articles
        url: posts
      - name: Auteurs
        url: auteurs
      - name: Genres
        url: genres
      - name: Editeurs
        url: editeurs
      - name: Tags
        url: tags
      - name: Séries
        url: series

  # home-info mode
  homeInfoParams:
    Title: "Mes lectures 📚"
    Content: Mes livres

#  socialIcons:
#    - name: x
#      url: "https://x.com/"
#    - name: stackoverflow
#      url: "https://stackoverflow.com"
#    - name: github
#      url: "https://github.com/"

  cover:
    hidden: true # hide everywhere but not in structured data
    hiddenInList: true # hide on list pages and home
    hiddenInSingle: true # hide on single page

  editPost:
    URL: "https://gitlab.com/-/ide/project/dans-les-nuages/livres/edit/main/-/content"
    Text: "Editer" # edit text
    appendFilePath: true # to append file path to Edit link

  # for search
  # https://fusejs.io/api/options.html
  fuseOpts:
    isCaseSensitive: false
    shouldSort: true
    location: 0
    distance: 1000
    threshold: 0.4
    minMatchCharLength: 0
    limit: 10 # refer: https://www.fusejs.io/api/methods.html#search
    keys: ["title", "permalink", "summary", "content"]

menu:
  main:
    - identifier: posts
      name: Articles
      url: /posts/
      weight: 10
    - identifier: auteurs
      name: Auteurs
      url: /auteurs/
      weight: 10
    - identifier: editeurs
      name: Editeurs
      url: /editeurs/
      weight: 20
    - identifier: series
      name: Séries
      url: /series/
      weight: 30
    - identifier: genres
      name: Genres
      url: /genres/
      weight: 40
    - identifier: tags
      name: Tags
      url: /tags/
      weight: 50


# Read: https://github.com/adityatelange/hugo-PaperMod/wiki/FAQs#using-hugos-syntax-highlighter-chroma
pygmentsUseClasses: true
markup:
  highlight:
    noClasses: false
    # anchorLineNos: true
    # codeFences: true
    # guessSyntax: true
    # lineNos: true
    # style: monokai
